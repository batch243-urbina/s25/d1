console.log(`Introduction to JSON`);

// [SECTION] JSON OBJECTS

// JavaScript Object Notation (JSON) a data format used by applications to store and transport data to one another.
// - not limited to JS-based applications and can also be used in other programming languages
// - file extension of (.json)
// - used in other programming languages hence the name JSON
// - core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
// - used for serializing different data types
// Syntax:
// {
// 	"propertyA": "valueA",
// 	"propertyB": "valueB"
// }

// [SECTION] JSON Object sample

// {
//   "city": "Quezon City",
//   "province": "Metro Manila",
//   "country": "Philippines",
// }

// // JSON ARRAYS:
// [
//   {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"
//   },

//   {
// 		"city": "Manila City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	}
// ];

// [SECTION] JSON METHODS
// - json object contains method for parsing and converting data into stringified JSON

let batchesArr = [
  {
    batchName: "Batch X",
  },
  {
    batchName: "Batch Y",
  },
];

console.log(`This is the original Array: `);
console.log(batchesArr);

// STRINGIFY METHOD - used to convert JavaScript objects into a string
console.log(`Result from stringify method: `);

let stringBatchesArr = JSON.stringify(batchesArr);
console.log(stringBatchesArr);
console.log(typeof stringBatchesArr);

let data = JSON.stringify({
  name: "John",
  address: {
    city: "Manila",
    country: "Philippines",
  },
});
console.log(data);

// [SECTION] USING STRINGIFY METHOD WITH VARIABLES
// when information is stored in a variable and is not hardcoded into an object that is being stringified, we can supply the value with a variable

// User details
// let firstName = prompt(`What is your first name?`);
// let lastName = prompt(`What is your last name?`);
// let age = prompt(`What is your age?`);
// let address = {
//   city: prompt(`Which city do you live in?`),
//   country: prompt(`Which country does your city address belong?`),
// };

// let otherData = JSON.stringify({
//   firstName,
//   lastName,
//   age,
//   address,
// });
// console.log(otherData);

// [SECTION] CONVERTING STRINGIFIED JSON into JAVASCRIPT OBJECTS
// objects are common data types used in application because of the complex data structures that can be created out of them
// information is commonly send to applications in stringified JSON and converted back into objects
// this happens both for sending information to a backend application and back to a frontend application

let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;

let parseBatchesJSON = JSON.parse(batchesJSON);
console.log(parseBatchesJSON);

console.log(parseBatchesJSON[0]);

let stringifiedObject = `{
	"name": "John",
	"age" : "45",
	"address" : {
		"city" : "San Fernando",
		"country" : "Philippines"
	}
}`;
console.log(JSON.parse(stringifiedObject));
